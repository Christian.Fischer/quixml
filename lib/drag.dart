import 'dart:io';

import 'package:desktop_drop/desktop_drop.dart';
import 'package:flutter/material.dart';

import 'edit.dart';
import 'models/data.dart';

class DragTargetArea extends StatefulWidget {
  const DragTargetArea({Key? key}) : super(key: key);

  @override
  _DragTargetAreaState createState() => _DragTargetAreaState();
}

class _DragTargetAreaState extends State<DragTargetArea> {
  late final Uri _uri;

  bool _dragging = false;

  @override
  Widget build(BuildContext context) {
    return DropTarget(
        onDragDone: (detail) {
          setState(() {
            _uri = detail.urls.first;
            _openFile();
          });
        },
        onDragEntered: (detail) {
          setState(() {
            _dragging = true;
          });
        },
        onDragExited: (detail) {
          setState(() {
            _dragging = false;
          });
        },
        child: SizedBox.expand(
          child: Container(
            color:
                _dragging ? Colors.blue.withOpacity(0.4) : Colors.blue.shade300,
            child: const Center(child: Text("Drop here")),
          ),
        ));
  }

  Future<void> _openFile() async {
    var file = File.fromUri(_uri);
    var data = await DataModel.create(file.path);
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => Edit(
                dataModel: data,
              )),
    );
  }
}
