import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:quixml/models/hive/item.dart';
import 'package:xml/xml.dart';
import 'dart:io';

class DataModel extends ChangeNotifier {
  /// Private constructor
  DataModel._create(XmlDocument doc, Box box) {
    _doc = doc;
    _box = box;

    // create hive
    var rootNode = Item()..node = _doc.rootElement.name.local;
    box.add(rootNode);

    recursiveAdd(_doc.rootElement);
  }

  void recursiveAdd(XmlElement element) {
    for (var node in element.children) {}
  }

  /// Public factory
  static Future<DataModel> create(String filePath) async {
    // Do initialization that requires async
    final file = File(filePath);
    final contents = file.readAsStringSync();

    final document = XmlDocument.parse(contents);

    Directory appDocDir = await getApplicationDocumentsDirectory();
    Hive.init(appDocDir.path);
    final box = await Hive.openBox('myBox');

    // Call the private constructor
    var component = DataModel._create(document, box);

    // Return the fully initialized object
    return component;
  }

  /// Internal, private state of the cart.
  XmlDocument _doc = XmlDocument();
  Box? _box;

  /// An unmodifiable view of the items in the cart.
  UnmodifiableListView<XmlElement> get items =>
      UnmodifiableListView(_doc.rootElement.childElements);

  XmlElement get root => _doc.rootElement;

  /// Adds [item] to cart. This and [removeAll] are the only ways to modify the
  /// cart from the outside.
  void add(XmlElement item) {
    //_items.add(item);
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }
}
