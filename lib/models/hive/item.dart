import 'package:hive/hive.dart';

@HiveType(typeId: 0)
class Item extends HiveObject {
  @HiveField(0)
  String node = "";

  @HiveField(1)
  String value = "";

  @HiveField(2)
  Item? parent;
}
