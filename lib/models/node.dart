import 'package:animated_tree_view/animated_tree_view.dart';

class CustomIndexedNode extends ListenableIndexedNode<CustomIndexedNode> {
  CustomIndexedNode([String? key]) : super(key: key);
}
