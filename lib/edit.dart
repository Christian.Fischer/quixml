import 'package:quixml/models/data.dart';
import 'package:animated_tree_view/animated_tree_view.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class IndexedRowItem extends ListenableIndexedNode<IndexedRowItem> {
  IndexedRowItem([String? key]) : super(key: key);

  void notify() {
    notifyListeners();
  }
}

class Edit extends StatefulWidget {
  const Edit({Key? key, required this.dataModel}) : super(key: key);

  final DataModel dataModel;

  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<Edit> {
  static const _showRootNode = true;

  final controller = IndexedTreeViewController<IndexedRowItem>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            IndexedTreeView<IndexedRowItem>(
              initialItem:
                  IndexedRowItem(widget.dataModel.root.name.toString()),
              controller: controller,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              showRootNode: _showRootNode,
              builder: (context, level, item) => buildListItem(level, item),
            ),
            if (!_showRootNode)
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: RaisedButton.icon(
                    onPressed: () => controller.root.add(IndexedRowItem()),
                    icon: const Icon(Icons.add),
                    label: const Text("Add Node")),
              ),
            const SizedBox(height: 32),
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget buildListItem(int level, IndexedRowItem item) {
    final color = colorMapper[level.clamp(0, colorMapper.length - 1)]!;
    return Card(
      color: color,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            ListTile(
              title: Text(
                "Item ${item.level}-${item.key}",
                style: TextStyle(color: color.byLuminance()),
              ),
              subtitle: Text(
                'Level $level',
                style: TextStyle(color: color.byLuminance().withOpacity(0.5)),
              ),
              trailing: !item.isRoot ? buildRemoveItemButton(item) : null,
            ),
            if (!item.isRoot)
              FittedBox(
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    buildAddItemChildButton(item),
                    buildInsertAboveButton(item),
                    buildInsertBelowButton(item),
                  ],
                ),
              ),
            if (item.isRoot)
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  buildAddItemChildButton(item),
                ],
              ),
          ],
        ),
      ),
    );
  }

  Widget buildAddItemChildButton(IndexedRowItem item) {
    return Padding(
        padding: const EdgeInsets.only(right: 16.0),
        child: TextButton.icon(
            icon: Icon(Icons.add_circle, color: Colors.green[800]),
            label: Text("Child", style: TextStyle(color: Colors.white)),
            onPressed: () => item.add(IndexedRowItem())));
  }

  Widget buildInsertAboveButton(IndexedRowItem item) {
    return Padding(
        padding: const EdgeInsets.only(right: 16.0),
        child: TextButton.icon(
            icon: Icon(Icons.add_circle, color: Colors.green[800]),
            label: const Text("Insert Above",
                style: TextStyle(color: Colors.white)),
            onPressed: () => item.parent?.add(IndexedRowItem())));
  }

  Widget buildInsertBelowButton(IndexedRowItem item) {
    return Padding(
        padding: const EdgeInsets.only(right: 16.0),
        child: TextButton.icon(
            icon: Icon(Icons.add_circle, color: Colors.green[800]),
            label: Text("Insert Below", style: TextStyle(color: Colors.white)),
            onPressed: () {
              final index =
                  item.parent?.indexWhere((node) => node.key == item.key);
              if (index != null) {
                item.parent?.insert(index, IndexedRowItem());
              }
            }));
  }

  Widget buildRemoveItemButton(IndexedRowItem item) {
    return Padding(
        padding: const EdgeInsets.only(right: 16.0),
        child: TextButton.icon(
            icon: Icon(Icons.delete, color: Colors.red[800]),
            label: Text("Delete", style: TextStyle(color: Colors.white)),
            onPressed: () => item.delete()));
  }
}

final Map<int, Color> colorMapper = {
  0: Colors.white,
  1: Colors.blueGrey[50]!,
  2: Colors.blueGrey[100]!,
  3: Colors.blueGrey[200]!,
  4: Colors.blueGrey[300]!,
  5: Colors.blueGrey[400]!,
  6: Colors.blueGrey[500]!,
  7: Colors.blueGrey[600]!,
  8: Colors.blueGrey[700]!,
  9: Colors.blueGrey[800]!,
  10: Colors.blueGrey[900]!,
};

extension ColorUtil on Color {
  Color byLuminance() =>
      this.computeLuminance() > 0.4 ? Colors.black87 : Colors.white;
}
